package net.devslash.vend.vendit.app

import android.os.Debug
import android.support.design.widget.Snackbar
import android.view.View

/**
 * Created by Paul on 6/12/2015.
 */

class HomeActionManager(val homePresenter: HomePresenter) : View.OnClickListener {

    override fun onClick(v: View?) {
        if (v != null) {
            Snackbar
                    .make(v, "Hi there!", Snackbar.LENGTH_LONG)
                    .show();
            homePresenter.actionButtonClicked(v)
        }

        if (Debug.isDebuggerConnected()) {
            Snackbar
                    .make(v, "Problem. No view", Snackbar.LENGTH_LONG)
                    .show()

        }
    }

}

interface HomePresenter {

    fun actionButtonClicked(v: View)

}
