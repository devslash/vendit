package net.devslash.vend.vendit.app

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class EntryActivityFragment : Fragment() {

    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var adapter: MainRecycleAdapter

    private lateinit var fAction: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.entry_fragment, container, false)
        val recycle = view.findViewById(R.id.entry_recycle_view) as RecyclerView
        fAction = view.findViewById(R.id.entry_frag_action_button) as FloatingActionButton
        fAction.setOnClickListener(HomeActionManager(HomePresenterImpl()))


        // This makes it faster as we don't reorient
        recycle.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(activity)
        recycle.layoutManager = layoutManager


        adapter = MainRecycleAdapter(arrayOf("hi", "there", "boi"))
        recycle.adapter = adapter

        return view
    }
}
