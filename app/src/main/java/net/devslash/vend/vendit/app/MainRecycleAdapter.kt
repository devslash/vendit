package net.devslash.vend.vendit.app

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

class MainRecycleAdapter(headings: Array<String>) :
        RecyclerView.Adapter<MainRecycleAdapter.MainViewHolder>() {

    val dataset = headings

    // THis is called when we have the layout and need to bind a value
    override fun onBindViewHolder(holder: MainViewHolder?, position: Int) {
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MainViewHolder? {
        val v = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.card_preview, parent, false) as LinearLayout
        val holder = MainViewHolder(v)
        return holder
    }

    override fun getItemCount(): Int {
        return dataset.size
    }


    class MainViewHolder(var item: View) : RecyclerView.ViewHolder(item) {
    }
}

