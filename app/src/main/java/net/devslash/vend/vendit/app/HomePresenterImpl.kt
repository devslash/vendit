package net.devslash.vend.vendit.app

import android.support.design.widget.Snackbar
import android.view.View

/**
 * Created by Paul on 6/12/2015.
 */

class HomePresenterImpl: HomePresenter {

    override fun actionButtonClicked(v: View) {
        Snackbar
            .make(v, "Hey! You clicked my button", Snackbar.LENGTH_SHORT)
            .show()
    }

}
